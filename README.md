# AZRS-TRACKING

Ovo je projekat iz kursa Alati za razvoj projekta na Matematickom fakultetu. Primena alata je koriscena na projektu [Stratego](https://gitlab.com/JovanS98/10-stratego) koji je radjen na kursu Razvoj softvera.

## Alati koji su primenjeni:

- CMake
- Clang Tidy
- Clang Format
- Clang Analyzer
- Valgrind
- Git Hook
- GCov
- GammaRay
- Doxygen
- Docker
- Git
- GDB

Primena alata koji su korisceni moze se videti [ovde](https://gitlab.com/JovanS98/azrs-tracking/-/issues/?sort=created_date&state=all&first_page_size=20).
